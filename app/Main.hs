module Main (main) where

data Tree a = Leaf a | Node Int (Tree a) (Tree a) deriving (Show)

data Digit a = Zero | One (Tree a) deriving (Show)

type RAList a = [Digit a]

size :: Tree a -> Int
size (Leaf _) = 1
size (Node n _ _) = n

node :: Tree a -> Tree a -> Tree a
node t1 t2 = Node (size t1 + size t2) t1 t2

fromT :: Tree a -> [a]
fromT (Leaf x) = [x]
fromT (Node _ t1 t2) = fromT t1 ++ fromT t2

fromRA :: RAList a -> [a]
fromRA = concatMap from
  where
    from Zero = []
    from (One t) = fromT t

xs = [One (node (Leaf 'a') (Leaf 'b'))]

main :: IO ()
main = undefined